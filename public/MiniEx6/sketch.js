let netSize={ //Setting the width and height for the picture net
  w:80,
  h:80
};
let min_corona = 5;  //minimum number of corona patients on the screen
let corona = []; // The array of corona patients
let net; // the image of the net
let hospital; // The image of the hospital (background)
let netPosX; //The x position of the net
let max_width; //Max width of the canvas
let min_width; // minimum width of the canvas
let score =0 //The score starts at 0
let lose = 0; // The number of lost patients starts at 0


//Pre loading the images (the net and the background)
function preload(){
  net = loadImage('./data/net.png');
  hospital = loadImage('./data/ScaryHospital.jpg')
}

function setup() {
  createCanvas(600, 400);

  //starts with 5 corona patients on the screen
  for (let i = 0; i <= min_corona; i++){ //
  corona[i]= new Corona ();
  }

//Gives the variables a start value
  netPosX= 200;
  max_width= 600
  min_width=0

}

//Making the corona patients class
class Corona{
  constructor(){
    this.speed = random(0.5,1.5);
    this.size = 30,30;
    this.pos = new createVector(random(30,width-30),0);
  }

//Deciding its behaviour of movement
move() {
    this.pos.y = this.pos.y + this.speed;

  }

//Deciding its behaviour of show - how it should look
show(){

//The green ellipse head
    stroke(158, 161, 153);
    fill(194, 245, 66);
    ellipse(this.pos.x,this.pos.y,this.size, this.size);
//eyes and mouth
    fill(0)
    noStroke();
    ellipse(this.pos.x-10, this.pos.y-5, this.size-25, this.size-25);
    ellipse(this.pos.x+10, this.pos.y-5, this.size-25, this.size-25);
    ellipse(this.pos.x,this.pos.y+7,this.size-20, this.size-20);
  }
}

function draw() {
  background(hospital); //Setting the background image

//The behaviour (the movement and the show) of the corona patients are looping
for (let i = 0; i <corona.length; i ++) {
  corona[i].move();
  corona[i].show();

  //If the patients cross the bottom-line (is bigger than the height of the canvas)
  //it should be removed (so that the computer know it has to send a new one in so there is min_corona)
  if(corona[i].pos.y>height){
   corona.splice(i,1);
    }
  }

//Calling the functions
  checkCoronaNum();
  checkCatch();
  showScore();
  checkResult();

//Drawing the image of the net
  image(net,netPosX,320,netSize.w, netSize.h);
}

//The function that checks the number of the corona patients on the screen
//Pushes out a new one each time the number is less that 5 (min_corona)
function checkCoronaNum() {
  if (corona.length < min_corona) {
    corona.push(new Corona());
  }
}

//Deciding how to controll the net by pressing the keys, and how far the net is moving for each time the key is pressed
function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    netPosX -= 34;
    }
  else if (keyCode === RIGHT_ARROW) {
    netPosX += 34;
    }

//Controlling the max and min width of the net (it can't move outside the canvas)
  if(netPosX<min_width){
    netPosX=min_width;
    }
  if(netPosX>=max_width){
    netPosX=500;
    }

}

//Function for checking when the net catches the corona patient
function checkCatch(){
  for (let i =0;i<corona.length;i++){

    //For each time the y-position of the corona patients is in the upper part of the net (in the opening)
    //and the x-position of the corona patients are in inside the width of the net
    // the score will raise by one, and the corona patiens will disapear
    if (corona[i].pos.y>320 && corona[i].pos.y<340){
        //print('x: ' + corona[i].pos.x + ' y: ' + corona[i].pos.y);
      if(corona[i].pos.x>netPosX && corona[i].pos.x<(netPosX+netSize.w)){
        score++;
        corona.splice(i,1);
      }
    }

    //If the corona patients moves by the net without being caught, and reaches the
    //y-position of 398 (almost the buttom-line) the lose counter will be raised by one
    if(corona[i].pos.y >= 398){
      lose++;
    }
  }
}

//The function that shows the score in the upper left corner
function showScore(){
  stroke(158, 161, 153);
  noFill();
  strokeWeight(2);
  textSize(15);
  rect(1,0,597,50)
  noStroke();
  fill(255);
  //The text for the scores and loses
  text('You have caught   '  + score +   '   corona patient(s)',5, 20);
  text('You have let          '+ lose +'   corona patiens free. They will now infect you and many others',5,35 );

}

//The function that checks the result and tells the user GAME OVER
function checkResult() {
  if (lose > score+3 && lose > 6) {
    fill(255,0,0);
    textSize(35);
    text('Too many corona patients got away', 20, 170);
    textSize(40);
    text('The world will now end', 90, 230)
    noLoop(); // the game stops and the user has to reload to start over
  }
}
