![ScreenShot](MiniEx6.JPG)

[Link to Rumme](http://annika.nh1.gitlab.io/ap-2020/MiniEx6/)

[Link to folder](https://gitlab.com/annika.nh1/ap-2020/-/tree/master/public/MiniEx6/)


**My program**

My MiniEx is a Corona-game which is about catching the infected corona patients in a net and put them in quarantine, before they infect other people. Very much related to the current world situation. The game consists of a class of corona patients falling from the top of the screen in different speed and positions. The user is controlling the net with the left and right key and is supposed to catch the falling corona patients in the net. In the left upper corner is a counter that counts the corona patients that is caught in the net, and the corona patients that get away and is not caught in the net. When there are 3 more corona patients that get away than there is caught, the game stops, and the user loses and must reload the page to start over and try again. 
 
In my MiniEx I have constructed my own class (the corona patients). I have been choosing some properties and behaviour for my object. The properties are e.g. speed and position. The behaviour is e.g. Moving down along the x-axis, and how it should be shown (green, and with an open mouth). I have in that process been choosing and not choosing to show certain details and tried to make a concrete model of the corona patient object. 
 
**Object-oriented programming and the wider implications of abstraction**
In real world, we can distinguish between classes and objects. Classes are abstract and objects are concrete. E.g. the class "animals" and the class "dogs" in the class of animals is abstract, and my own dog "Søren" is concrete. But when talking about computation and programming, objects will also be an abstraction, as it is not something real world object, but something we have constructed. In short, object abstraction is about how physical objects can be digitalized, and can be translated to something, that the computer understands. 

There is different layer of abstraction in computing. E.g. there are the low levels of abstraction that is expressed in binary language, and then there are high level of abstraction as for example the graphical user interface or high-level programming languages as Phyton, Java and Javascript. In this MiniEx I am working with a high level of abstraction while focusing on object abstractions.  
 
When we are working with object-oriented programming we are dealing with the complexity of objects and we are trying to make a concrete model of them. In this translation from something complex to a concrete model of an object, we as programmers make choices about specific parts or details of an object to include and which parts or details not to include. That makes our objects to abstractions. 
 
At the same time, that makes object-orientated programming and object construction to a sociotechnical practise, as Matthew Fuller & Andrew Goffey also points out in their text: "The Obscure Objects of Object Orientation". This is because the process isn’t objective but biased by our choices and thereby the environment around us, and how we see the world. We as programmers chooses how we want people around us to see and think off our constructed object. 
 
In the text by Matthew Fuller & Andrew Goffey they also underline the fact, that the relation between things is a central part of OOP: "Objects are generated as instances of ideal types or classes, but their actual behaviour is something that arises from the messages passed to them from other objects". Classes and objects give a lot of new possibilities for referring from e.g. one class or object to another and makes it a lot easier to reuse objects. 
 
 
**Extend/connect - digital example from the real world**
A real-word example of abstraction is e.g. Facebook. Peoples Facebook-profiles are in a way an abstraction of themself. On a Facebook profile there are a profile-picture and some information about e.g. age, abode and interests and in that way a complex human is being abstracted to just being this profile. Here a lot of details about a person are being left out. People become a digital abstraction of a real-world object.
