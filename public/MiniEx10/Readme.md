![ScreenShot](Flowchart.jpg)

*My individual flowchart of my MiniEx 6 object game*

[Link to Miniex 6 object game](https://gitlab.com/annika.nh1/ap-2020/-/blob/master/public/MiniEx6/Readme.md)




[Link to group flowcharts 1](https://gitlab.com/Signe.Tvilling/ap-2020/-/blob/master/public/MiniX10/Flowchart1.1.pdf)
[Link to group flowcharts 2](https://gitlab.com/Signe.Tvilling/ap-2020/-/blob/master/public/MiniX10/Flow2.2.pdf)



Algorithms can be described in many ways. A classic way to think about algorithms might be in a technical sense, that algorithms is a kind of recipe behind the program. A step-by step guide to how the program is build. But algorithms are, as Taina Bucher argues, manifold. They are multiple things at once. They are not just technical and functional, but social and cultural as well. Social e.g. in the way that they affect people’s lives on more levels and influence the way they act and think of the world. Algorithms can be cultural in the way they are not neutral, but they represent certain ideologies or values. 
 
Another way to describe algorithms is through a flowchart. Nathan Ensmenger describes flowcharts as: "represent the conceptual structure of complex software systems". They can be used as a way of communication of a program or a conceptual idea - as an object to talk with. At the same time, they can be used as a blueprint that the program can be constructed on the basis of. 

 
In this week’s MiniEx I have used flowcharts in various ways. In groups we have made flowcharts to plan and develop our conceptual idea, before we have made our program. When we made our flowcharts, we decided some rules, that the program must be built upon. By making such a flowchart we are forced to think critical on the rules we decide and why we want it to work exactly like that. At the same time, the flowcharts can help us with the structuring when we begin programming. Furthermore, these flowcharts can be used to communicate our conceptual idea to others, so they can understand our idea and maybe provide feedback on it. The flowcharts we made are not very detailed, as our conceptual idea is not yet fully developed. That might make the concept more difficult to grasp. 
 
 When i made a flowchart of my object-game from MiniEx 6, I used the flowchart as a way to communicate the algorithmic procedure in a simpler way, so that other people trying it are able to understand how it works technically. As my individual flowchart was made after the game was programmed, the flowchart is much more detailed. That might be more confusing, but at the same time necessary to understand the technical process.
 
 
So, in a way the flowchart has to be simple to communicate ideas or thoughts to someone that does not know anything about it, but at the same time it has to communicate quite complex algorithmic procedures. These things can be difficult to combine, but by using a flowchart it makes this process a lot easier, than explaining it in another way. It is very visual, but at the same time with explanatory short texts. That makes flowcharts a smart tool to get a fast overview. 
 
 



**The technical challenges for our 2 ideas**

My group and I would like to work with the theme about Data capturing. Our first idea is based on data from a Json-file, that we want to write ourselves. The Json file are supposed to contain a lot of text/ data about our peers. This idea is challenging, as it might take a lot of time to collect all this data, but if we succeed in this, it might turn out to be a very cool program.
 
Our second idea is based on Data from an API from Google analytics. This idea is challenging, as it is necessary that we run our program on a website to use Google analytics. None of us has worked with websites before, but we will try to acquaint ourselves with this. At same time we need to figure out how to use the Google analytics API. So far that seems quite difficult to figure out, but luckily Google has a lot of guides. 
 
 