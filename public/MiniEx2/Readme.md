![ScreenShot](MiniEx2.PNG)

[Link to Rumme](http://annika.nh1.gitlab.io/ap-2020/MiniEx2/)

[Link to MiniEx2 folder](https://gitlab.com/annika.nh1/ap-2020/-/tree/master/public/MiniEx2/)

I have made two environmentalist emojis. The first emoji I designed was the crossed out cow. The cow consists of a body made out of a rectangle syntax, and black blotches made from bezier syntaxes. 
The cows head is made from a rectangle and a quad, and its ears are made with the bezier syntax as well. I worked with some new and more difficult syntaxes, compared to last mini ex, such as the Beziers and quads syntaxes. 
It was challenging to keep track of all the augments in both of the shapes and it took me a lot of time to get them to appear like I wanted them to. The factory is made out of rectangles and triangles and the smoke is made from ellipses.
When making the smoke, I used the extra alpha argument to make it transparent. When making the cow I used the extra arguments to the shapes, to make shapes with rounded corners.
To make the red crosses I used the text syntax and made a big X. When working with both of my emojis, I used the translate and scale syntaxes to place them in the right spot, and to make them equal sized.

Both of my emojis are very simple drawings made out of simple shapes and with out a lot of details, but at the same time it is obvious what they are supposed to depict. The emojis are mostly made from angular shapes, such as rectangles and triangles. 
To make the emojis more realistic, I also used some soft shapes as the beziers and ellipses. Each kind of shape has its own function.

I chose these emojis to underline the current environmental focus. Protecting the environment is probably one of the biggest issues in politics as well as on social media among common people. 
Often a lot of people write articles and posts on social media concerning environmental issues, but a the moment there is no emojis representing this. 
That is why I have made a crossed out cow, symbolizing that eating meat (especially cow-meat) is bad for the environment and a crossed out factory symbolizing that pollution or Co2 is bad for the environment as well.  

The text; "Modifying the Universal", deals with issues concerning representation, diversification and the idea of universality when talking about emojis. The text is especially focusing on the troubles with skin-colour.
My emojis are not centered around the skin-colour topic, but instead around other kinds of representation. 
A lot of interests and hobbies are represented in Apples emojis like eg. surfing, photography, trompet-playing and acting, whereas the environment activism interest is not represented.
My emojis are therefore supposed to represent  environmentalists as well as vegetarians. 