function setup() {
  // put setup code here
  createCanvas(800,480);

}
function draw() {
background(164, 245, 152);

//translate(-160,100);
translate(0,50);

//COW

fill(255);
//Legs
rect(440,170,9,73,20,20);
rect(600,171,8,74,20,20);
rect(460,168,8,74,20,20);
rect(583,173,8,71,20,20);


//Body
fill(255);
stroke(0)
rect(427,69,192,110,20,20);

//Spots
fill(0);
bezier(510,68,477,82,609,116,567,68);
bezier(618,84,574,81,563,156,619,164);
bezier(516,180,517,139,566,152,577,179);
bezier(523,104,403,94,519,182,514,133);
bezier(517,103,477,111,582,87,512,140);
bezier(523,104,403,94,519,182,514,133);
bezier(427,123,434,104,477,135,427,161);
//Head
fill(255)
quad(390, 49, 450, 49, 440, 120, 400, 120);
fill(254,208,199);
rect(398, 95, 45, 25,5,5);
fill(0)
ellipse(410,107,8,8);
ellipse(430,107,8,8);

//Eyes
fill(95,94,94);
ellipse(405,70,13,5);
ellipse(435,70,13,5);
//Horns
fill(195,179,176);
triangle(440,23,437,49,423,49);
triangle(400,49,410,49,400,18);

//Ears
fill(255);
bezier(365,20,435,55,378,70,375,41);
bezier(455,20,470,55,430,70,440,41);

//Cross
fill(255,0,0);
textSize(230);
text('X', 440, 200);


//FACTORY
push();
scale(0.70);
translate(150,55);
fill(118,117,116);
//Hous
rect(50,250,230,140);
//Chimneys
rect(50,70,40,180);
rect(100,130,20,120)
//Roofs
triangle(130,250,200,250,130,200);
triangle(210,250,280,250,210,200);
//Smoke
stroke(89,89,89);
fill(117,113,113,200);
ellipse(60,60,20,20);
ellipse(80,60,20,20);
ellipse(70,60,20,20);
ellipse(70,45,20,20);
ellipse(60,40,20,20);
ellipse(70,35,20,20);
ellipse(60,25,20,20);
ellipse(75,25,20,20);
ellipse(60,15,20,20);
ellipse(60,0,20,20);
ellipse(60,-20,20,20);
ellipse(70,10,20,20);
ellipse(60,-30,20,20);
ellipse(60,-15,20,20);
pop();
//Cross
stroke(0);
fill(255,0,0);
textSize(230);
text('X',140, 320);

}
