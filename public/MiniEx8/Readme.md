![ScreenShot](MiniEx8.JPG)

[Link to Rumme](https://helene.boeriis.gitlab.io/ap2020/MX8/)

[Link to MiniEx8 folder](https://gitlab.com/annika.nh1/ap-2020/-/tree/master/public/MiniEx8/)


**Helene & Annika**

**Title of artwork: wordsOfMyFeelings**

Short description of the thoughts behind our work

The focal point of our artwork is feelings and emotions. When running the program you have to click on the floating words and make a sentence of how you feel in the current situation. 
We got inspired by this untypical situation of lockdown we all are in these days. We have to be locked down in our houses and apartments, where we have to keep a social distance from our loved ones. The whole country is a lockdown and a life indoor is our reality for most of us. While we are working from home, face timing our friends and calling our grandparents, a lot of people make sure that our welfare system is prepared for the unexpected repercussions of the virus. 

When our every day is changed indefinitely we discover a set of new feelings. Feelings you may never have had before or have been intensified. Feelings of being alone, isolated or worried. Based on the current situation we have selected a set of feelings/words so you can describe how you feel. You can write a verse and affect the meaning of the program based on the current  situation, your mood and feelings. The floating words are supposed to symbolize the many thoughts or worries in people's minds.


**Description of the code and program**

We have named the variables in your source code to explain which feelings you have to catch and think upon how they affect your mood. See line 90 in the source code. 
When clicking on a word there will be a deep sound, to underline the interaction and the impact the user has on the work. The words on the canvas are loaded from a JSON file to our program. We used a for loop to print the words on the canvas and used the random syntax to make them move randomly around the canvas. We made arrays with the words start values (x and y) to make them spread all over the canvas, and we used if-statements to make the words move into the canvas again each time they hit the sides, bottom or top of the canvas. To check if the user of the program clicks on a word, we used a for loop to check the mouse x and mouse y’s distance to each of the words from the JSON file. We made an array for the sentence the user is writing, so that each word the user is clicking on is added to a row of words, that in the end makes a sentence. 

**Our process**

For this mini ex we found it hard to re-thinking and re-organize our thoughts when trying to write code as poetry and focusing on the source code and not only the runme. Two weeks ago we learned about object-oriented programming, which aims to make the source code more manageable and somehow reduces the source code. We have in general learned how to reduce our source codes with for loops and variables for keeping the source code in a manageable composition. In contradistinction to our previous mini exes, we had to / or had the opportunity to create poetry in our source code so it in combination with the runme will have a concerted expression. So, now the source code isn’t what is behind the runme but a part of it as well. It was difficult to make lines of code with no actual meaning for the runme, but only for the expression in total, but definitely, a good exercise to re-think a source code and how it can be used for poetry and emotional expressions. We have named our variables so they fit to our runme and makes it more connected. 


**Analyze and articulate**


**Code and poetry - related to the text:  “The aesthetics of generative code” by Geoff Cox and Alex McLean**
In the text “The aesthetics of generative code” they discuss what makes poetry and code aesthetic, and in that continuation, which similarities there are between the two. Originally the aesthetic theory has described experience by what is perceived with the senses. According to Kant both the intellect and the senses has to combine to fully get an aesthetic experience, and that's what the text argues, is needed to appreciate generative code. The viewer of code has to “‘sense’ the code to fully grasp what it is we are experiencing and to build an understanding of the code’s actions” (p. 7). That is similar to poetry, as the aesthetic value of poetry also not only lies in the written form, but in the way it is spoken, its tempo and rhythm, as well. A poet has to be executed/ performed as well as code does. In our program, the aesthetic value lies partly in the written form, as we have tried to name our variables and functions to make the written form more aesthetic, and as a part of the final work. Still the name of the variable does not express our intention fully when the lines of poetry are quite short as an outcome of the difficulties in writing poetry in the source code. When the program is executed, it has to be combined with the executed form, to fully grasp the experience of the work. 
Our program changes a lot while being executed - in part because of the interaction as well as the random movement of the words. The code is happening in real-time, and according to the text, poetry does the same; “it is always in the process of becoming” (p. 6)  and it unfolds in real-time.
The text says: “Evidently, code works like poetry in that it plays with structures of language itself, as well as our corresponding perceptions”(p. 6). Our code has a lot of structure, and consists of code language as well as natural language. The order is crucial to make the program executable in the intended way. It is the same with poetry. The way a poet is structured - where there is a line break or a repetition - has a big influence on the way it is executed or performed. The context or the overall structure has importance to the understanding of poetry as well as code. Both poetry and code get perceived differently of each individual reading/ listening to it, or watching it being performed. 

**Reflection upon various layers of voices and/or the performativity of code in your program**
It can be argued that our program has a different layer of voices. It has the source code which in itself has a voice in its written form - it is double code, as it is written in both programming language as well as natural language. The code is expressing something in itself and is communication to the humans reading it. The executed program has a voice as it consists of words to combine to sentences describing feelings and moods. Our material is the text in written form, and the words (their expression and appearance) gives form to the work.  In addition to that our program has another voice that stands for expressions and has a message. It is important to talk to people around you about this whole situation and which feelings it brings to you. 
It can be said that code is performative as it consists of instructions that can be executed. There are similarities between code and language and in the source code there can be meanings of natural language. The instructions and the executions are happening at the same time when talking about code - that is not necessarily the same with speech. For example we can distinguish between the illocutionary and the perlocutionary speech of act. The illocutionary speech of act is performed together with the words spoken, and the perlocutionary speech of act triggers a chain of effects and the consequences of the speech do not perform at the same time (Geoff Cox & Alex McLean, Vocable Code, 2013). 


