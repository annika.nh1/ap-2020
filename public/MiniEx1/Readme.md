**Read me MiniEx1**


I coded a picture of a little town, with some houses, a tree, and a car driving by.
I used some of the shape syntaxes and I tried out a lot of colors. In addition to that, I worked with getting the car to move along the axis of x. 
Final I figured out how to make shapes transparent. 
 
My first coding experience was fun and very challenging. While coding I was thinking a lot, to understand what was going on, and to figure out what would happen if I for example changed this number etc.
I found out, that it was difficult to use the codes written by other people and use it myself, because it was difficult to figure out, what they had done and why they had done in that way. 
Therefore, I started from scratch and only wrote my own code, but of course I was using the reference list a lot, and I got some inspiration from other peoples work on the internet. 
Generally, I found my first experience nice, but also very protracted, because it took me a lot of time, just to make a very few illustrations, but hopefully I will get faster over time. 
  
I think coding has a lot of similarities with writing text because when you program you must "spell correctly" if you want the computer to understand what you are writing. 
That is the same when you are writing, if you want your readers to understand what you are saying, you must spell correctly and make the correct structure of the sentence. 
On the other side coding is less free, because you must use some predetermined commands if you want it work, and when writing you can write whatever makes sense to you. 
 
I think programming can teach me a lot of other abilities than just to write code. It forces me to think differently from what I usually do. 
As Nick Montfort says, programming can lead to a lot of cognitive advantages such as logical thinking and planning skills, and that programming brings pleasure, because programmers is “adding something to the world”. 


1![ScreenShot](Miniex1.PNG)

[link to Rumme](http://annika.nh1.gitlab.io/ap-2020/MiniEx1/)

I can't get my link to work, and i can't fingure out why. I hope to get some help with it soon.
