![ScreenShot](MiniEx5.PNG)

[Link to Rumme](http://annika.nh1.gitlab.io/ap-2020/MiniEx5/)

[Link to MiniEx5 folder](https://gitlab.com/annika.nh1/ap-2020/-/tree/master/public/MiniEx5/)


I have reworked my design from MiniEx1 both technically and conceptually. Before the changes, my Miniex1 didn’t really have a concept except just being a colorful childish picture of a little town. Now, my work has got a more considered idea.
 
In my program there are a lot of colorful houses, a road with some cars driving by, some people walking, and a sun that moves across the sky. In addition to that a button that says: "Click to freeze time" is placed in the top, and when the button is pressed once all movement stops. When the button is pressed again the movement continues. 
 
I have developed my work in the way, that it has gotten more details and more movement. In addition to that, it has become interactive. 
I have tried to use some of the syntaxes that has caused me trouble, and some that i haven’t used before. That is e.g. the for loop, which i used to make the stripes on the road as well as all the small houses in the background. Furthermore, I have worked a lot with the conditional statements such as if-statements and Boolean expressions. Just as in the last MiniEx I worked with a button - I styled it with CSS and worked with making functions that is running when the button is pressed.
 
By using all these new syntaxes, I managed to make a much better concept of my MiniEx1. The for loops made it more detailed and realistic, the if-/ else-statements made the cars not just drive by, but continually driving by again and again, that is the same when it comes to he sun. The Boolean expression made it possible for me to control the movement by means of the button. 
 
All this led me to make my work focus on time. All the continual and repeated movement of people walking, cars driving by, and the sun crossing the sky are to symbolize that time passes. I myself think a lot about time in my everyday life. All the time I plan my time. Time after time I get surprised of how time is flying, and sometimes I even get annoyed with the time, either because I am not able to do what I had planned, or because I waste it on something stupid. That is why, at least I, sometimes really could use a button to stop the clock and catch up with time. In my work I have tried to portray this time subject in a simple and accessible way.
 


Vee mentions in her text, that literacy gains its meaning and power in social interactions, and that might be the same with programming. If we just learned programming by just trying to remember and learn as many syntaxes as possible, a dimension of programming would be missing. Aesthetic programming can help us to put programming into a deeper perspective and context, and to think through programming in relation to our culture and society around us. 
 
Vee talks about "Literacy no longer implies just reading for comprehension, but also reading for critical thought […] (Vee 2017, p. 48). Aesthetic programming can teach us to use programming as a tool to reflect critically about the world. Programming is a way to express oneself, and if the message one sends has a critical point of view on something, that might lead to changes and development. In continuation of this programming can be seen as a method for design as it is a new format for expression, and it opens up a way to be creative. 

