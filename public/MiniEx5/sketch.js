
//Cars movement
var x =0;
var c =-300;
var g = -600;

var sun = 0 //sun movement on x-aksis
var sun2 = 50 //sun movement on y-aksis
var h //Humans movement
var button;
var freeze = true; //boolean expression to control the stop and start of the movement

function setup() {
  createCanvas(700,480);

  //Button
  button=createButton('Click to make time freeze');
  button.position(290,25);
  button.size(100,50);
  //When the button is pressed the movement stop
  button.mousePressed(stop);

  //Styling button in CSS
    button.style("color","#fff");
    button.style("padding","5px 8px");
    button.style("background","#fc0303");
    button.style("border","none");
}

function draw() {
background(255, 204, 0);

//Call function sky (blu sky and sun)
sky();
//Call function road
road();
//Call function houses
houses();
//Call function Tree
tree();
//Call function Human
human();
//Call function Cars
cars();
//Call function Movement
movement();
}



function movement(){

if(freeze==true){
//Orange car is driving again and again
if(x<width){
x=x+1;
}
else(x=-50);

//yellow car drives again and again
if(g<width){
g=g+2;
}
else(g=-50);

//blue car drives again and again
if(c<width){
c=c+4;
}
else(c=-50);

//Sun moving along the x-aksis
if(sun<700){
sun=sun+0.3;
}
else(sun=0);

//sun moving along the y-aksis
if(sun2<700){
sun2=sun2+0.01;
}
else(sun2=400);
}

if(freeze==true){
//Human walks by
if(h<1000){
h=h+0.5;
}
else(h=0);
}
}


//function to make the picture freeze
function stop(){
freeze=false
button.mousePressed(start);
}

//function to make the picture move again
function start(){
  freeze=true;
  button.mousePressed(stop);
}

function tree(){

  //Tree
  fill('rgb(138,93,67)')
  rect(450,180,30,130)

  //Leaves
  fill(0, 150, 0, 200);
  circle(445, 190, 50);
  circle(485, 190, 50);
  circle(465, 155, 50);
}
function cars(){
  //Orange Car
  fill('rgb(255,165,48)')
  rect(30+x, 320, 100, 65);
  rect(130+x, 335, 50, 50);

  fill(51);
  ellipse(50+x, 389, 40, 40);
  ellipse(120+x, 389, 40, 40);


  //Blue car
  fill(0,0,255);
  rect(30+c, 320+20, 100, 65);
  rect(130+c, 335+20, 50, 50);

  fill(51);
  ellipse(50+c, 389+20, 40, 40);
  ellipse(120+c, 389+20, 40, 40);


  //yellow car
  fill(255, 204, 0);
  rect(30+g, 320+40, 100, 65);
  rect(130+g, 335+40, 50, 50);

  fill(51);
  ellipse(50+g, 389+40, 40, 40);
  ellipse(120+g, 389+40, 40, 40);
}

function road(){

  //The road
  fill('rgb(176,170,181)');
  rect(0, 350, 700, 200);

  //The stribes on the road
  for (var s = 0; s <= width; s=s+100) {
    fill(255);
    rect(s,410,55,10);
  }

}

function sky(){
  //Blue sky
  fill('rgb(43,170,255)');
  rect(0, 0, 700, 200);

  //Sun
  fill('rgb(255,249,66)')
  ellipse(sun, sun2, 70, 70);
}

function houses(){
  //blue houses in the background
  push();
  for (var h = 0; h <= width; h=h+100) {
    fill(color(0, 0, 255));
    square(h, 150, 50);
    fill(255,0,0);
    triangle(h, 150, h+25, 110, h+50, 150);
  pop();
  }

  // pink houses in the background
  push();
  for (var l = 40; l <= width; l=l+100) {
    fill('#fae')
    square(l, 180, 50);
    fill(255, 204, 0);
    triangle(l, 180, l+25, 140, l+50, 180);
  pop();
  }

  // Red houses in the background
  push();
  for (var r = -20; r <= width; r=r+100) {
    fill(255,0,0);
    square(r, 180+20, 50);
    fill('rgb(157,115,255)');
    triangle(r, 180+20, r+25, 140+20, r+50, 180+20);
  pop();
  }

  //Blue house - nr. 1
  fill(0,0,255);
  square(30, 180, 100);
  fill(255,0,0);
  triangle(30, 180, 130, 180, 86, 90);

  //pink house nr. 2
  fill('#fae')
  square(140, 180, 110);
  fill(255, 204, 0);
  triangle(140, 180, 250, 180, 200, 90);

  //red house nr. 3
  fill('rgb(100%,0%,10%)');
  square(260, 180, 120);
  fill('rgb(157,115,255)');
  triangle(260, 180, 380, 180, 320, 90);
}
function human(){
  //Human
  push();
  scale(0.7);
  translate(-600,140);
  fill(0);
  ellipseMode(CENTER);
  ellipse(600+h,250,30,30);
  rectMode(CENTER);
  rect(600+h,300,35,65,5,5);
  strokeWeight(5);
  line(590+h,335,583+h,380);
  line(610+h,335,620+h,380);
  line(590+h,275,560+h,310);
  line(610+h,270,630+h,310);
  pop();

  //Human 1
  push();
  scale(0.5);
  translate(-700,350);
  fill(0);
  ellipseMode(CENTER);
  ellipse(600+h,250,30,30);
  rectMode(CENTER);
  rect(600+h,300,35,65,5,5);
  strokeWeight(5);
  line(590+h,335,583+h,380);
  line(610+h,335,620+h,380);
  line(590+h,275,560+h,310);
  line(610+h,270,630+h,310);
  pop();

  //Human 1
  push();
  scale(0.5);
  translate(-750,350);
  fill(0);
  ellipseMode(CENTER);
  ellipse(600+h,250,30,30);
  rectMode(CENTER);
  rect(600+h,300,35,65,5,5);
  strokeWeight(5);
  line(590+h,335,583+h,380);
  line(610+h,335,620+h,380);
  line(590+h,275,560+h,310);
  line(610+h,270,630+h,310);
  pop();

  //Human 1
  push();
  scale(0.7);
  translate(-400,140);
  fill(0);
  ellipseMode(CENTER);
  ellipse(600+h,250,30,30);
  rectMode(CENTER);
  rect(600+h,300,35,65,5,5);
  strokeWeight(5);
  line(590+h,335,583+h,380);
  line(610+h,335,620+h,380);
  line(590+h,275,560+h,310);
  line(610+h,270,630+h,310);
  pop();
}
