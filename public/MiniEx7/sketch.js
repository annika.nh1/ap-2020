
var CanvasSizeX = 650;
var CanvasSizeY = 650;
var x = 10;
var y = 10;
var h = 30; //height of ellipse
var w = 30; // width of ellipse
var d = 25 // distance between rows, culums and ellipses
var r = 0; // amount of rounds


function setup() {
  createCanvas(CanvasSizeX, CanvasSizeY);
  background(255);
  frameRate(2);
}

function draw() {

//The thikness and the colour of the ellipses
  strokeWeight(2.5);

//Number of rows are being incremented by one
  r = r+1;

//setting the x and y starting point.
  y = r*d;
  x = r*d;

/*Rows from top left to top right
r*d : each time a new row is made, the row will move a distance d and a row towards the middle*/
  while(x<CanvasSizeX-r*d){
      x=x+d;
      //fill(random(50,255), random(50,255), random(50,255));
      noFill();
      ellipse(x,y,(random(10,40)),h);
    }

    //setting the x and y starting point
    x=CanvasSizeX-r*d;
    y=r*d;

//coloums from top right to bottom right
  while(y<CanvasSizeY-r*d){
      y=y+d;
      noFill();
      ellipse(x,y,(random(10,40)), h);
  }

//setting the x and y starting point
    x=CanvasSizeX-r*d;
    y=CanvasSizeY-r*d;

//rows from buttom right to bottom left
  while(x>0+r*d){
    x=x-d;
    noFill();
    ellipse(x,y,(random(10,40)),h);
  }

  //setting the x and y starting point
  x=0+r*d
  y=CanvasSizeY-r*d

  //coloums from bottom left to top left
  while(y>0+r*d){
      y=y-d;
      noFill();
      ellipse(x,y,(random(10,40)),h);
    }

  /*When there has been 13 rounds, the ellipsis reaches the middle*/
    if(r==13){
      r=0; //number of round starts over
      background(255); //Background clears the screen

      //The colourof the circles changes randomly
      stroke(random(0,255),random(0,255),random(0,255));
    }
}
