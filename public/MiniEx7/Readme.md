![ScreenShot](MiniEx7.JPG)

[Link to Rumme](http://annika.nh1.gitlab.io/ap-2020/MiniEx7/)

[Link to MiniEx7 folder](https://gitlab.com/annika.nh1/ap-2020/-/tree/master/public/MiniEx7/)


**My program**
 
When I made my program I started from scratch, and I didn’t base my program on anything already existing (which I have often done in my other MiniEx's). I started making up some rules that gave me the basic behaviour of my program, and afterwards I worked with making it more interesting to look at by working with the sizes, shapes and the colours. 
 
These are the rules I based by program on:
 
•	Keep making ellipses until the side of the canvas is reached. Turn 90 degree and make ellipses until the side of the Canvas. Continue to the starting point is reached. Ending up forming a square. 
 
•	Second round the rows of ellipsis will follow a square inside the previous
 
•	When the squares reach the middle, the screen is cleared and the pattern starts over in a new randomly picked colour
 
I have used while loops for making the ellipses again and again until the condition is not correct anymore. I have used an if-statement for making the canvas clear and start over each time the canvas is filled up with squares of ellipses. My program is both predictable in the way, that it loops over and over again, but at the same time unpredictable, as I have used the random syntax to make the ellipses width changing, and the program colours changing each time the canvas is cleared. My program is built upon rules and instructions I made up, and on the basis of that it auto-generates the rest of my work. Because of that, my program could be categorized as generative code. When working with generative code and auto-generation, I find it interesting, that just by changing on a few parameters the whole visual appearance can change significantly, and often change in a way, that I did not predict. By using the random or noise syntaxes the work can get unpredictable even though it is auto generated.

 
**Generative code, autonomy and randomness**

Generative art/ code is based on autonomy. A part of a generative artwork is in some way auto generated on the basis of instructions or rules. One can argue that generative art presents a new way of being an artist as it is both based on mathematical logic as well as creativity. When talking about the generative code you can say, that to produce it, you need a combination of humans and machines. You need the human to give the computer instructions, but you need the computer to execute them. But when giving the computer instructions, the outcome does not have to be predictable, you can give the computer instructions that result in unknow outcomes, as for example when using randomness in computing like with the program 10Print. 10Print makes a very complex and endless pattern and the program is just based on very few simple rules. 10Print is based on randomness as it is random if the program prints a forward slash or a back slash. In the example with Langton’s Ant, the initial steps are not based on randomness, but on mathematical rules more precisely just 2 simple rules. This also results in a very complex pattern. 
 
 In p5.js I have used the random function a lot of times, but I have not really thought about what it does and how. This week’s theme has opened my eyes to the fact, that randomness is more complex and has more layers. A computer can never be fully random. The random function comes with varying outputs, but is still based on an algorithm, so over time, there will always be some kind of pattern in the way the computer picks random values. That might be the same with humans - human’s brains are not capable of making things random without creating patterns in the randomness. 
 
The p5.js has the syntax called noise, which is based on Perlin Noise. Perlin Noise is another kind of randomness, where each output is related to the previous as opposed to when using the random syntax. Perlin noise gets a more natural and organic look when used visually. 
