![ScreenShot](MiniEx4.PNG)

[Link to Rumme](http://annika.nh1.gitlab.io/ap-2020/MiniEx4/)

[Link to MiniEx2 folder](https://gitlab.com/annika.nh1/ap-2020/-/tree/master/public/MiniEx4/)


**Provide a title of your work and a short description of the work (within 1000 characters) as if you are submitting to the festival open call.**

 
*It's free and always will be*
 
My work is called It's free and always will be. It is a preview on how Facebook in the nearest future highly possible will work when creating a new profile. This will be reality because Facebook, at that time will collaborate and work even more together with external websites, and therefore even before the future user have created an account, Facebook will already have their data, and can fill out the Sign Up form just based on their face. The algorithm behind will identify the users previously used e-mail, name, birth date and so on based on face-recognition from pictures of you other places on the internet. The title It's free and always will be that Facebook writes on their Sign Up page, is to point out, that when the service is free you are the product yourself. In this context you use the social platform for free, but you "sell" your data. 
 
**The text The Like Economy: Social Buttons and the Data-Intensive Web by Carolin Gerlitz and Anne Helmond**
 
The text: The Like Economy: Social Buttons and the Data-Intensive Web  focuses on Facebook and its social buttons. The text explains how Facebook is a social web, as it consists of a participatory and collaborative production of content, and relations on Facebook are created by users linking to other web objects such as pictures, profiles, websites and so on. It point out how Facebook has established the Like economy which is based on selling and using the valuable consumer data that Facebook collects. Facebook then creates an infrastructure where the social interactivity, as for example likes, are tuned into users data. They are suddenly not only a social platform but also a data fabric. All user affects and interactions are instantly measured for the purpose of the data to create even more traffic. The text introduces the notion "The open graph", which allows other websites to link to Facebook via eg. Like buttons. This is to enable more social web engagement and traffic. The open graph is making so that external websites all can work together and map users the connections. 
 
**Describe your program and what you have used and learnt.
Articulate how your program and thinking address the theme 'capture all'.**

 
My program consist of images of the Facebook header and the Facebook Sign Up page, and is supposed to depict exactly the Sign Up page, where you create an account. On the Facebook Sign up page, instead of just filling in the form, you can choose to just click on the button "Show my information". When you as a user click, the face tracker will start tracking you, and on the basis of that, it will fill out your sign up form with the data it can collect about you other places on the internet, and therefore create your account based on your face, without you giving any information yourself. 
 
To make the webcam and the tracker work, I have installed a new liberary and used the createCapture and clm.tracker syntaxes. To make the button, I have worked with the different p5.js button syntaxes and the CSS styling of the button. To make something happen when the button is clicked, i have made a new function, that starts running, and interrupt the draw loop. The function starts the face tracker and shows the text "Scanning face…". I wanted my program to scan for a while before creating an account, and to do that, I used the frame count, and made another variable called Scanframes = 0. I put the Frame counts equivalent to the Scanframes and subtracted the Scanframes from the frame count. When the frame count, from when the button is clicked, reaches 180, the account is created.
 
To make this program I used a lot of new things I had not used before, such as images, Boolean expression, the web cam and face tracker and the button syntaxes to make it interactive. Using so many new syntaxes caused me a lot of trouble. Especially when positioning the webcam, the face tracker and canvas correctly in correlation with each other. The whole issue with deciding where to place different blocks of code - if it should be in setup or draw was difficult for me as well. Everything considered I think I learned a lot from making this, and I also think it turned out fine.
 
**What are the cultural implications of data capture?**
 
The tracking and data capture on the internet, and especially on Facebook, has a lot of negative implications for the society as well as the individuals. First of all it means no privacy, the social media platforms know more than you do your self. They know private things as eg. your political and religious persuasion. If this data is used in the wrong way, it can have fatale consequences. For example the wrong people knowing your religious persuasion in a country without freedom of religion, can mean persecution. The wrong people knowing your political persuasion or where you might be in doubt about something can eg. lead to targeted campaigns or fake news and therefore affect parliamentary elections. On the other hand the data capture can seem quite smart as it recommends you just the movies on Netflix, that you like, and shows you advertisements of stuff you want to buy. That can actually make it easier for you at some points, but at the same time it can manipulate you and reduce your knowledge about the outside world. 

