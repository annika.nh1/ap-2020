
let ctracker; //tracker variable
let img; //header image
let imgto; //create account image
let imgtre //Create account image with information
let button;
let scanon = false; //Boolean expression - Scanon (the face scan)
let scanframes=0; //when mouse is pressed the scanframes are 0

//the function that preloades the pictures
function preload() {
img = loadImage('./data/SignupforFacebook.jpg');
imgto = loadImage('./data/SignupforFacebook2.jpg');
imgtre = loadImage('./data/SignupforFacebook3.jpg');
}

function setup() {

  //setup camera capture
  let cam = createCapture(VIDEO);
  cam.size(640, 480);
  cam.position(50, 280);

  // setup canvas
  let cnv = createCanvas(1500, 1500);
  cnv.position(0, 0);

  // setup tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(cam.elt);

//Styling button in CSS
  button = createButton('Show my information');
  button.style("color","#fff");
  button.style("padding","5px 8px");
  button.style("background","#4c69ba");
  button.style("border","none");

//Sets buttons position and size
  button.position(800, 600);
  button.size(200,50);

//choosing the function that is going to start when the button is pressed
  button.mousePressed(startscan);

  //noFill();
}

function draw() {

//When button is pressed the scanning starts
if(scanon==true){
  //It clears everytime draw runs so that the red spots want stay on the screen
  clear();
  // get array of face marker positions [x, y] format
  let positions = ctracker.getCurrentPosition();
  // If there are positions
  if (positions) {
  // Positions come as an array of 2-item arrays where 0 is x, 1 is y
    for (let pos of positions) {
      let x = pos[0]+50;
      let y = pos[1]+280;
      stroke('red');
  // draw ellipse at each position point
      ellipse(x, y, 1, 1);
    }
  //The text "scanning face" is shown when the button is pressed
  stroke(40, 85, 181);
  fill(40, 85, 181);
  textSize(50);
  text('Scanning face...',100, 240);

  }

}

//The pictures of Facebooks signups page
image(img,0,0,windowWidth,150);
image(imgto,690,150,600,400);

//When the button is pressed the framecount has to be counted and the scanframes has to be subtracted
// to find the framecount after the button is pressed.
// This is to decide how long time the scanner scans before creating an account
if (scanframes !=0 && frameCount - scanframes >180){
//when the framecount reaches 180 the account is created

//draw rectangle to hide the "scanning face" text
fill(255);
noStroke();
rect(60,165,620,100);

//Write new text: Your account is now created
stroke(40, 85, 181);
fill(40, 85, 181);
textSize(35);
text('YOUR ACCOUNT IS NOW CREATED',80,240);

fill(255);
noStroke();
rect(717,256,195,40);

//Draw image of the users information
image(imgtre,690,150,600,400);

}



}

//The function that runs when the button is pressed. The variable scanon has to be true to start the scanning
function startscan(){

scanon=true;
scanframes=frameCount;

}
