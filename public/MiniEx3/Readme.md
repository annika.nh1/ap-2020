![ScreenShot](MiniEx31.PNG)
![ScreenShot](MiniEx32.PNG)
![ScreenShot](MiniEx33.PNG)

[Link to Rumme](http://annika.nh1.gitlab.io/ap-2020/MiniEx3/)

[Link to MiniEx3 folder](https://gitlab.com/annika.nh1/ap-2020/-/tree/master/public/MiniEx3/)

I have created a 3 different throbbers that shifts when the framcount reaches 20, and then again when it reaches 40. The 3 throbbers a supposed to be shown tree different times, the webpage is loading.

The first throbber represent a cup of coffee, the second throbber a snack, and the last throbber is a human streching himself. More figures and texts could be added to the row of throbbers each time the webpage is loading again.

The thoughts behind the throbbers are, that the figures and the text in the different throbbers, are giving ideas to things to do while waiting, so that the waiting time does not feel so unproductive and long. 

To make my  throbbers I have used a lot of shape syntaxes such as rectangle, triangles, lines, quads and beziers. In addition to that I have used the text syntaxes to write a shot text to each one of them. Concerning time-related syntaxes, I have used FrameRate, which is a time-related syntax because it decides the number of times pr. second the draw function runs. I have set the FrameRate to 2, to control how fast the ikons are rotating, and i have chose this speed because, I think it shows progress, and that something is going on behind the screen, but at the same time the speed is not so fast, that it looks like you don't have the time to eg. go get a cup of coffee. 

Furthermore I have used the Framecount syntax, which is a time related-syntax, that counts the number of times drawing has looped. I use this syntax to position the next object in the circle, of figures, as well as to decide when the throbber on the screen shifts to the next one. 

The throbbers i have made, are a lot like the trobbers we normally sees on the internet, in the way that it does not show how long time the user have to wait. The speed of the throbber running does not reflect the speed of the processing either. The only things the throbbers do, are to communicate to the user, that something is loading by constantly moving and not just freeze on a webpage. 


As we read in the text; Throbber: Executing Micro-temporal Streams, throbbers are a cultural phenomenon, only for the purpose of keeping the users busy and informing the users about the handling of data behind the screen. The text mentions serveral times, how the loading time of the throbber appears wasted and unproductive and that the situation just consumes the users time. In continuation of this,  the text points out, that smoothness of network traffic is very important to the user experience and the users time perception. 

The thing that separate my throbbers from the once on eg. Youtube or Netflix is, that they encourage the user to use the waiting time on something else and in that way be more productive. This would hopefully mean less frustration from the perspective of the user, and at the same time it would allow and remind the user to take a break and stand up. All things considered this might end up in a better user experience.  
