

function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight);   //create a drawing canvas
   frameRate (2);  //try to change this parameter


}
function draw() {

//The background has a blue colour and a transperant alpha value
  background(149, 216, 230, 80);

//The first 20 framecounts the program draws the Coffee
//the next 20 framecounts that program draws the Snack
//The last framecounts the program draws the Human

if(frameCount<20){
    drawCoffee();

console.log(frameCount);
  }

if(frameCount>20&&frameCount<40){
drawSnack();
}

if(frameCount>40){
drawHuman();
}
}

//defines the function for the coffee
function drawCoffee() {
//The text
  push();
  textSize(30);
  fill(59, 43, 9);
  text('GO GET A CUP OF COFFEE', 565, 110);
  pop();

//the code that makes the throbber rotate
  let num =8; // The figure rotate 8 times before getting back to start
  translate(width/2, height/2); //move things to the center
  //360/num >> degree of each ellipse's movement; frameCount%num >> get the remainder that indicates the movement of the ellipse
  let cir = 360/num*(frameCount);  //to know which one among 8 possible positions.
  rotate(radians(cir)); // to rotate

  //The coffee cup
push();
  strokeWeight(6);
  stroke(59, 43, 9);
  noFill();

//the cups handle
  bezier(126,90,158,68,140,144,120,116);
  strokeWeight(2);

//the steam from the cup
  bezier(118,10,93,21,142,50,118,60);
  bezier(98,10,73,21,122,50,98,60);
  bezier(78,10,53,21,102,50,78,60);

// The cup
  noStroke();
  fill(59, 43, 9);
  quad(66,75,70,130,120,130,130,75);
  pop();
}

//Defines the function for the Snack
function drawSnack(){

//The text
  push();
  textSize(30);
  fill(240, 65, 138);
  text('GO GET A SNACK', 635, 110);
  pop();

//the code that makes the throbber rotate (look at the Drawcoffee function to see desciption)
let num =8;
translate(width/2, height/2); //move things to the center
  //360/num >> degree of each ellipse's movement; frameCount%num >> get the remainder that indicates the movement of the ellipse
let cir = 360/num*(frameCount);  //to know which one among 8 possible positions.
rotate(radians(cir));

//The snack
push();
fill(240, 65, 138);
noStroke();
  rect(40,70,30,50,5,5);
  triangle(55,80,70,50,35,50);
  triangle(70,140,55,110,35,140);
pop();
}

// defines the function for the human
function drawHuman(){
//The text
push();
textSize(30);
fill(240, 144, 65);
text('GO STRETCH YOURSELF', 585, 110);
pop();

//the code that makes the throbber rotate
let num =8;
translate(width/2, height/2); //move things to the center
    //360/num >> degree of each ellipse's movement; frameCount%num >> get the remainder that indicates the movement of the ellipse
let cir = 360/num*(frameCount);  //to know which one among 8 possible positions.
rotate(radians(cir));

push();
//The Human
//The body and head
fill(240, 144, 65);
noStroke();
ellipse(40,70,30,30);
rect(27,90,30,50,5,5);

//The arms and legs
stroke(240, 144, 65);
strokeWeight(4);
line(27,100,-5,70);
line(55,100,80,70);
line(34,140,25,200);
line(50,140,60,200);
pop();
}
